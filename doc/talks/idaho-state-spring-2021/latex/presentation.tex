%12.8cm x 9.6cm   Is default size

\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\title{\large{AN OPTIMAL ALGORITHM FOR SELECTION ON $X+Y$: LAYER-ORDERED HEAPS AND THE CURRENT FASTEST ISOTOPE CALCULATOR FOR CHEMICAL COMPOUNDS}}
\date[date]{\today}
\author{OLIVER SERANG//\\
  ASSISTANT PROFESSOR//\\
  COMPUTER SCIENCE//\\
  UNIVERSITY OF MONTANA}

\usetheme{texsx}
\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame} 
  \frametitle{\large CARTESIAN PRODUCT: $X$ ALL THE $Y$}
  \includegraphics[width=4.25in]{../figures/x-all-the-y.jpg}
\end{frame}

\begin{frame} 
  \frametitle{\Large A PROBLEM FROM EVERYDAY LIFE}
  $X$ are the prices of food from a menu, $Y$ are the prices of drinks, the smallest $k$ values from $X+Y$ are the cheapest $k$ meals. \emph{E.g.}, we'd like to generate the cheapest $k$ meals and see if any of them are what we want to eat.

  -----------------------------------\\

  HOT SPRING PHO, IDAHO FALLS, ID\newline

  \tiny
  \begin{tabular}{rl}
    \begin{minipage}{2in}
      FOOD (\emph{i.e.}, $X$):\\
      \begin{tabular}{rl}
        Nem Nuong & \$8.25\\
        Goi Cuon & \$7.25\\
        Bun Thit Nuong Cha Gio & \$14.75\\
        Tom Cuon Chien & \$9.55\\
        Muc Chien Bot & \$10.75\\
        Bun Dac Biet & \$16.75\\
        \vdots        
      \end{tabular}
    \end{minipage}
    &
    \begin{minipage}{2in}
      DRINK (\emph{i.e.}, $Y$):\\
      \begin{tabular}{rl}
        Tra Nong & \$1.95\\
        Caphe & \$4.95\\
        Soft Drink & \$1.95\\
        Sinh To & \$7.25\\
        Sua Dau Nanh & \$3.50\\
        Juice & \$2.50\\
        \vdots        
      \end{tabular}
    \end{minipage}
  \end{tabular}


  %% \vspace{0.5in}
  %% ``Why does man kill? He kills for food. And not only food: Frequently there must be a beverage.''\\ -Woody Allen 
\end{frame}

\begin{frame} 
  \frametitle{\huge FORMAL PROBLEM DEFINITION}

  Let $X$ and $Y$ be lists of length $n$.\\
  Let $Z = (X_i+Y_j ~|~ \forall i,j)$.\\
  Let $Z'$ be sorted: $Z_t \leftrightarrow Z'_s ~:~ Z'_1 \leq Z'_2 \leq \cdots$.\\
  Given some integer $k$, find the values $Z'_1, Z'_2, \ldots Z'_k$ in any order.\newline

  (Variant: also find the $i,j$ from which each $Z'_s = X_i+Y_j$ was computed.)
\end{frame}

\begin{frame} 
  \frametitle{\Large EXAMPLES FROM SCIENTIFIC COMPUTING}

  {\large \bf Chemistry:}\\
  $X$ are the log abundances of isotopes of calcium. $Y$ are the log abundances of isotopes of chlorine. The $k$ largest values from $X+Y$ give us the $k$ most abundant isotopes of CaCl.\newline

  {\large \bf Statistics:}\\
  $X$ and $Y$ are two random variables, and we want to approximate the distribution of $X+Y$ using only the $k$ most probable values.\newline

  {\large \bf Numeric Linear Algebra:}\\
  Given two matrices $X$ and $Y$, approximate the Kronecker product $X \otimes Y$ by using its most significant $k$ terms. 
\end{frame}

\begin{frame} 
  \frametitle{\huge LOWER BOUND ON COMPLEXITY}

  It is necessary to load $2n$ values and return $k$ values; therefore, any solution to this problem will have runtime $\in \Omega(n+k)$.\newline

  If we get an algorithm that runs $\in O(n + k)$, it's \emph{optimal}. 
\end{frame}

\begin{frame} 
  \frametitle{\huge HOW DIFFICULT IS SORTING?}

  If we have a list of $n$ values, and we sort by comparing them with the $<$ operator, how fast can we possibly sort them?
\end{frame}

\begin{frame} 
  \frametitle{\huge GUESS WHO? GAME}

  \center
  \includegraphics[width=4in]{../figures/guess-who-game.jpg}
\end{frame}

\begin{frame} 
  \frametitle{\huge THE MAGIC OF DIVIDING IN HALF}

  \center
  \includegraphics[width=3in]{../figures/tastykake.png}
\end{frame}

\begin{frame} 
  \frametitle{\huge COMPLEXITY OF SORTING}
  \small
  The number of comparisons needed to go from $n!$ unsorted lists to 1
  sorted list (assuming unique values, the worst-case difficulty),
  is $\log_2(n!)$.

  \begin{eqnarray*}
    \log_2(n!) &=& \log_2(1\cdot 2\cdot 3\cdots n)\\
    &=& \log_2(1) + \log_2(2) + \cdots + \log_2(n)\\
    &\geq& \log_2\left(\frac{n}{2}\right) + \log_2\left(\frac{n}{2}+1\right) + \cdots + \log_2\left(n\right)\\
    &\geq& \log_2\left(\frac{n}{2}\right) + \log_2\left(\frac{n}{2}\right) + \cdots + \log_2\left(\frac{n}{2}\right)\\
    &=& \frac{n}{2} \log_2\left(\frac{n}{2}\right)\\
    &\in& \Omega(n \log(n))
  \end{eqnarray*}
\end{frame}

\begin{frame} 
  \frametitle{\huge NAIVE APPROACH VIA SORTING}
  Generate $n^2$ values, sort, retrieve minimal $k$ values.\newline

  \[r(n) = n^2 \log(n^2) + k = n^2\cdot 2 \log(n) + k \in \Theta(n^2 \log(n))\]
\end{frame}

\begin{frame} 
  \frametitle{\Large IS SORTING \emph{NECESSARY} FOR SELECTION?}
  \footnotesize
  \begin{tabular}{rl}
    \begin{minipage}{1.8in}
      \includegraphics[width=1.8in]{../figures/median-of-medians.pdf}
    \end{minipage}
    &
    \begin{minipage}{1.8in}
      \includegraphics[width=1.8in]{../figures/median-of-medians-array.pdf}
    \end{minipage}
  \end{tabular}\newline
    \tiny


    %% Finding the $k^{\text{th}}$ smallest value without sorting via
    %% ``median-of-medians'' (Blum \emph{et al.} 1973): Divide into lists
    %% of size 5. Get median of each in $O(1)$ time
    %% ($O(n)$ total). Compute median of $\frac{n}{5}$ medians, $\tau$
    %% (shaded black).\newline

    %% There must be $\geq \frac{3 \frac{n}{5}}{2}$ values $\leq
    %% \tau$ (the values that must be $\leq \tau$ are colored red;
    %% there may be other values $\leq \tau$). There must be $\geq
    %% \frac{3 \frac{n}{5}}{2}$ values $\geq \tau$ (the values that
    %% must be $\geq \tau$ are colored blue; there may be other values
    %% $\geq \tau$).

    %% Iterate through and count all values $\leq \tau$ and count all
    %% values $\geq \tau$. Both of these must be $\geq\frac{3n}{10}$. Cut
    %% the side of the median-of-medians that does not include the
    %% $k^{\text{th}}$ smallest value (using the counts computed via
    %% iteration). Recurse on $\leq\frac{7n}{10}$.\newline
\end{frame}


\begin{frame} 
  \frametitle{\Large RUNTIME OF MEDIAN-OF-MEDIANS}

  We recursed on $\frac{n}{5}$ medians and $\leq\frac{7n}{10}$
  values. Together, this is $\leq\frac{9n}{10}$ values.\newline

  We can prove that the total cost of finding the
  $k^{\text{th}}$ smallest value is $\in O(n)$.\newline

  At any recursion depth, the size of all child recursions relative to
  the size of all parent recursions is $\leq\frac{9}{10}$; therefore,
  the total work is $\leq 2n\cdot(1 + \frac{9}{10} +
  {\left(\frac{9}{10}\right)}^2 + \cdots) \in O(n)$.
\end{frame}

\begin{frame} 
  \frametitle{\large NAIVE APPROACH VIA MEDIAN-OF-MEDIANS SELECTION}
  Generate $n^2$ values, use median-of-medians to compute $k^{\text{th}}$ value, partition.\newline

  \[r(n) \in \Theta(n^2)\]
\end{frame}

\begin{frame} 
  \frametitle{\Large MORE ADVANCED APPROACH: SORTING AXES}
  \footnotesize
  \begin{tabular}{rl}
    \tiny
    \begin{minipage}{2in}
      Separately sort axes $X$ and $Y$ in $\Theta(n \log(n))$
      time. Best result is $X_1+Y_1$. Second best is either $X_1+Y_2$
      or $X_2+Y_1$. Proceed in this manner, only considering $X_i+Y_j$
      after $X_{i-1}+Y_j$ or $X_i+Y_{j-1}$ is already included in the
      result.\newline

      Gray pairs indicate values included in the result, black
      indicate values currently considered as the next value, and
      white indicate values not yet considered.
    \end{minipage}
    &
    \begin{minipage}{1in}
      \includegraphics[width=1in]{../figures/axes-sorted.pdf}
    \end{minipage}
  \end{tabular}\newline

  The ``hull'' (black squares) are stored in a heap as tuples
  $(X_i+B_j,i,j)$. At each iteration, pop the next smallest value from
  that heap and insert its neighbors $(X_{i+1}+B_j,i+1,j)$ and
  $(X_i+B_{j+1},i,j+1)$ (if either is already in the heap, don't
  insert it again).\newline

  After $k$ pops, the size of the hull is at most $t = \min(n, k+1)$; therefore, the runtime of each insert/pop operation to the hull was $\in O(\log(t))$, and thus the runtime is $\in O(n\log(n) + k\log(t)) = O(n\log(n) + k\log(\min(n,k)))$.\newline
\end{frame}

\begin{frame} 
  \frametitle{\Large HEAP APPROACH (WITH UNSORTED AXES)} \footnotesize
  ``Heapify'' $X$ and $Y$: w.l.o.g., $X_i \leq X_{2i},
  X_{2i+1}$. This is done in $O(n)$. Start with $(X_1+Y_1,1,1)$ (the
  best result value) in the heap (order tuples in the heap
  lexicographically). When a value is popped, insert its nearest
  inferior ``dependent'' tuples, taking care not to insert multiple
  times:
  \[
  dependents(i,j) = 
  \begin{cases}
    \{(2i,1), (2i+1,1), (i,2), (i,3)\}, & j=1\\
    \{(i,2j), (i,2j+1)\}, & j>1.\\
  \end{cases}.
  \]

  Each pop removes one value from the heap and inserts between two and
  four values; therefore, after $k$ pops, the size of the heap is $\in
  [k, 3k]$. The runtime of each insert/pop operation to the hull was
  $\in O(\log(\cdot))$, and thus the total runtime (including
  heapification) is $\in \Theta(n + k \log(k))$.\newline

  Any method that retrieves the $k$ results in sorted order will be
  $\in\Omega(k\log(k))$; to get an optimal algorithm, which we proved
  must be linear in both $n$ and $k$, we must not sort axes, and we
  must not retrieve results in sorted order.
\end{frame}

\begin{frame} 
  \frametitle{\Large LAYER-ORDERED HEAP} \footnotesize

  \begin{tabular}{cc}
    {\bf HEAP} & {\bf LAYER-ORDERED HEAP}\\
    \includegraphics[width=2in]{../figures/heap.pdf} & \includegraphics[width=2in]{../figures/layer-ordered-heap.pdf}
  \end{tabular}

  In a heap, nodes can be better than the siblings of their parents
  (\emph{e.g.}, $4\leq 6$). In a layer-ordered heap, this isn't
  allowed: every element in level $\ell$ of the tree is $\leq$ to
  every element in level $\ell+1$.\newline

  The rank of the layer-ordered heap (LOH) is denoted using
  $\alpha$. This means how many children a node has on average (and
  therefore, the ratio of the subsequent layer's size to the current
  layer's size).\newline

  With $\alpha=2$, $(8,2,5,1,9,4,7)$ can be LOHified into $((1) \leq (4,2) \leq (5,9,8,7))$.
\end{frame}

\begin{frame} 
  \footnotesize

  \frametitle{\Large LAYER-ORDERED HEAP} \footnotesize Define a
  ``layer-ordered heap'' (LOH) to be a stricter form of heap. Whereas
  a standard heap has $X_i \leq X_{2i}, X_{2i+1}$ (\emph{i.e.}, the
  children of $X_i$ are not better than $X_i$), there is no ordering
  between the left-left grandchild of $X_i$ and the right child of
  $X_i$.\newline

  A LOH partitions $X$ into layers, where each layer $L_i$ is a
  collection of values from $X$ and where each layer has $L_i \leq
  L_{i+1}$. A LOH of rank $\alpha$ has layers that grow exponentially
  in size: $\alpha \approx \frac{|L_{i+1}|}{|L_i|}$.\newline

  {\bf CONSTRUCTION COST:} For $\alpha \gg 1$ (\emph{e.g.},
  $\alpha=2$), LOHify can be performed in $O(n)$: use
  median-of-medians to partition the largest layer of $X$ away from
  the remaining values. When $\alpha=2$, the largest layer will be
  slightly larger than the remaining values. Thus, the runtime from
  median-of-medians calls will be $\in O\left(n + \frac{n}{2} +
  \frac{n}{4} + \frac{n}{8} + \cdots \right) \in O(n)$.\newline
\end{frame}

\begin{frame} 
  \frametitle{LOH ALGORITHM FOR SELECTION $k$ MIN FROM $X+Y$}
  \tiny

  \begin{tabular}{cc}
    \begin{minipage}{1.8in}
      \includegraphics[width=1.8in]{../figures/layer-product-grid.pdf}
    \end{minipage} &
    \begin{minipage}{2in}
      1. Separately LOHify $X$ and $Y$ with $\alpha=2$ ($O(n)$ steps). They
      will each have $m \approx \log_\alpha(n)$ layers. Compute the
      minimum and maximum values in each layer ($O(n)$ steps).\newline
      
      2. Build a grid of ``layer products'' (LPs).\newline
      
      3. Sort min and max corners of LPs ($O(m^2\log(m)) =
      O(\log^2_\alpha(n) \log(\log_\alpha(n))) \subset o(n)$
      steps). Iterate through until the area of the max corners is
      $\geq k$ ($O(m^2) = O(\log^2_\alpha(n)) \subset o(n)$ steps).\newline

      4. Go through in sorted order counting the area of all LPs whose
      max corners you've visited (use $s$ to denote this area). Stop
      once $s \geq k$. Every LP with a min corner visited whose max
      corner is not counted in $s$ will also be considered. Use $s'$
      to denote the number of elements from these LPs.\newline

      5. Generate all elements from these LPs. Perform a 1D
      $k$-selection on the result via median-of-medians ($O(n+k)$ steps).
    \end{minipage}
  \end{tabular}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=0<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-0.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=0<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-1.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-2.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-3.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-4.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-5.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=3<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-6.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=3<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-7.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-8.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-9.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-10.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-11.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=9<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-12.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=13<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-13.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=17\geq k$, terminate.\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-14.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  \includegraphics[width=2.8in]{../figures/k-is-26-s.pdf}\newline

  \vspace{0.1in}
  \footnotesize
  First, we have largest value seen $\tau=10$, where we've counted $s=17$
  values are $\leq \tau$. Thus, at least 17 values are $\leq\tau$; therefore,
  \begin{eqnarray*}
    \tau &\geq& \text{ the } 17^{\text{th}} \text{ smallest value of }X+Y\\
    &\geq& \text{ the } 14^{\text{th}} \text{ smallest value of }X+Y.
  \end{eqnarray*}\newline
  So if we only consider values $\leq 10$, they should include the smallest 14 values. 
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  \includegraphics[width=2.8in]{../figures/k-is-26-s.pdf}\newline
  \footnotesize
  
  Denote the final LP's area as $\delta$.\\
  We have $s - \delta < k$ and\\
  $s \geq k$.\newline

  Note that the LP above the final LP, $(1,5)+(1)$, is counted in
  $s-\delta$. Our final LP has area $\alpha\times$ as large as the LP above, so
  $\delta \leq (s-\delta)\cdot \alpha < k\cdot \alpha$.\\
  Thus, $s = s-\delta~+~\delta < k + k\cdot\alpha\in \Theta(k)$.
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  \includegraphics[width=2.8in]{../figures/k-is-26-s-prime.pdf}\newline

  \tiny
  Consider the area of all LPs with any value $\leq \tau$ (this
  area is 33). Denote $s'$ as the number of values $\leq \tau$ not
  counted in $s$.\newline

  Consider the LP at layer index $(u,v)$, which is
  counted in $s'$.\newline

  {\bf Case: $u>1,v>1$}: We know that the sorted order must visit
  max-corner of $(u-1,v-1)$ before visiting the min corner of
  $(u,v)$. Thus, the max-corner at $(u-1,v-1)$ must be counted in
  $s$. These LPs with min corner at $(u,v)$ and max corner at $(u,v)$
  not counted by $s$ will have area $\leq \alpha^2 s \in
  O(k)$.\newline

  {\bf Case: $u=1$ or $v=1$}: W.l.o.g., the max corner of LP $(u-1,1)$
  precedes the min corner of LP $(u,1)$ in the sorted order. Visiting
  the max corner of LP $(u,1)$ counts its area in $s$. Thus, at most
  one LP with $v=1$ is counted by $s'$, and LP $(u-1,1)$ must be
  counted by $s$. Thus, for both $u=1$ or $v=1$, these LPs will have
  area $\leq 2 \alpha\cdot s \in O(k)$.
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}

  The LPs guaranteed to contain the minimal $k$ values have total area
  $\in O(k)$. Generate all of their $O(k)$ values and select the
  minimal $k$ using median-of-medians.\newline

  The resulting algorithm costs $2n$ to LOHify $X$ and $Y$ and
  $\Theta(k)$ to fetch the $k$ smallest values. It's overall runtime
  is $\in O(n+k)$, which is optimal!
\end{frame}

\begin{frame} 
  \frametitle{\large $X+Y$ selection code}

  \includegraphics[width=4in]{../figures/x-plus-y-selection-code.png}
\end{frame}
  
\begin{frame} 
  \frametitle{\large SELECTION ON $X_1+X_2+\cdots+X_m$}
  \footnotesize
  Build a tree of $X+Y$ nodes, where $X$ and $Y$ are generated as
  LOHs. From each $X+Y$ node, use the algorithm presented here to
  select the best $1,\alpha,\alpha^2,\ldots$, thereby producing a LOH
  output. This means every $X+Y$ node in the tree takes in two LOHs
  and outputs an LOH.\newline

  Interestingly, the number of values of $X$ and $Y$ to produce the
  best $k$ values of $X+Y$ is $k+1$. Consider a version with sorted
  axes:\newline

  \center
  \includegraphics[width=2.5in]{../figures/sorted-area-vs-axes.pdf}
\end{frame}

\begin{frame} 
  \frametitle{\large SELECTION ON $X_1+X_2+\cdots+X_m$}
  \scriptsize


  The LOH-based $X+Y$ selection algorithm with \emph{unsorted axes}
  works by overshooting the number of $X_i+Y_j$ values retrieved and
  then finishing with median-of-medians to get the best $k$. If we
  want area $k$, we may generate $\alpha^2\cdot k$ values; therefore,
  in the worst-case, we'll need $\alpha^2\cdot (k + 1)$ values from
  $X$ and $Y$ combined.\newline

  If we view the $X_1+X_2+\cdots+X_m$ selection algorithm as a tree, consider the most values we'd request at that depth:

  \begin{center}
    \tiny
  \begin{tabular}{rcl}
    Depth in tree && Total values requested at each depth\\
    \hline
    0 (root) && $k$\\
    1 && $\approx\alpha^2\cdot k$\\
    2 && $\approx\alpha^4\cdot k$\\
    3 && $\approx\alpha^8\cdot k$\\
  \end{tabular}
  \end{center}

  Total runtime is $n\cdot m$ to load all $X_i$ plus the cost of all $X+Y$ selections in tree:
  \tiny
  \begin{eqnarray*}
    &\leq& k + \alpha^2\cdot k + \alpha^4\cdot k + \alpha^6\cdot k + \cdots + \alpha^{2 \log_2(m)}\cdot k\\
    &=&k\cdot (1 + \alpha^2 + \alpha^4 + \cdots + \alpha^{2\log_2(m)})\\
    &=&k\cdot \frac{\alpha^{2\log_2(m)+2} - 1}{\alpha^2-1}\\
    &\approx&k\cdot \frac{m^{2\log_2(\alpha)} - 1}{\alpha^2-1}
  \end{eqnarray*}

  \footnotesize
  When $\alpha<\sqrt{2}$, we have an $o(n\cdot m + k\cdot m)$ algorithm to select the best $k$ from $X_1+X_2+\cdots+X_m$. 
\end{frame}

\begin{frame} 
  \frametitle{\large GENERATING MOST ABUNDANT ISOTOPE PEAKS}

  \tiny
  Use the $X_1+X_2+\cdots+X_m$ tree. At the leaves, generate multinomial
  terms. \emph{E.g.}, an element with four Carbons has polynomial
  $(0.9893\cdot X^{12.0} + 0.0107\cdot X^{13.003})^4$. My students and
  I built a similar algorithm to get the best $k$ values from a
  multinomial (not described here).

  \center
  \includegraphics[width=2.8in]{../figures/neutronstar.pdf}\newline

  \footnotesize
  (Live demo of {\tt NeutronStar}, the world's fastest exact isotope calculator) 
\end{frame}

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  \includegraphics[width=4.25in]{../figures/patrick-kreitzberg.JPG}

  Patrick Kreitzberg (PhD student in mathematics)
\end{frame} 

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  \includegraphics[width=2.5in]{../figures/kyle-lucke.JPG}

   Kyle Lucke (masters student in computer science)
\end{frame} 

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  \includegraphics[width=3.25in]{../figures/jake-pennington.JPG}

   Jake Pennington (masters student in mathematics)
\end{frame} 

\begin{frame} 
  \frametitle{ACKNOWLEDGMENTS}
  \footnotesize
  Thanks to the Idaho State University Department of Computer Science for the invitation!\newline
\end{frame}

\end{document}
