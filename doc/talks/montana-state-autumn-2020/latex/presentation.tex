%12.8cm x 9.6cm   Is default size

\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\title{\large{AN OPTIMAL ALGORITHM FOR GENERATING THE SMALLEST $k$ VALUES IN A CARTESIAN SUM}}
\date[date]{\today}
\author{OLIVER SERANG//\\
  ASSISTANT PROFESSOR//\\
  COMPUTER SCIENCE}

\usetheme{texsx}
\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame} 
  \frametitle{\huge $X$ ALL THE $Y$}
  \includegraphics[width=4.25in]{../figures/x-all-the-y.jpg}
\end{frame}

\begin{frame} 
  \frametitle{\huge PROBLEM DEFINITION}

  Let $X$ and $Y$ be lists of length $n$.\\
  Let $Z = (X_i+Y_j ~|~ \forall i,j)$.\\
  Let $Z'$ be sorted: $\forall t \exists ! s : Z_t = Z'_s \leq Z'_{s+1}$.\\
  Given some integer $k$, find the values $Z'_1, Z'_2, \ldots Z'_k$ in any order.\newline

  (Optional: also find the $i,j$ from which these $Z'_1, Z'_2, \ldots
  Z'_k$ were computed.)
\end{frame}

\begin{frame} 
  \frametitle{\huge EXAMPLE FROM EVERYDAY LIFE}
  $X$ are the prices of food from a menu, $Y$ are the prices of drinks, the smallest $k$ values from $X+Y$ are the cheapest $k$ meals. 
\end{frame}

\begin{frame} 
  \frametitle{\Large EXAMPLES FROM SCIENTIFIC COMPUTING}

  {\large \bf Chemistry:}\\
  $X$ are the log abundances of isotopes of calcium. $Y$ are the log abundances of isotopes of chlorine. The $k$ largest values from $X+Y$ give us the $k$ most abundant isotopes of CaCl.\newline

  {\large \bf Statistics:}\\
  Two distributions $X$ and $Y$. Want to approximate the distribution on $X+Y$ using only the $k$ most probable values. 
\end{frame}

\begin{frame} 
  \frametitle{\huge LOWER BOUND ON COMPLEXITY}

  It is necessary to load $2n$ values and return $k$ values; therefore, any solution to this problem will have runtime $\in \Omega(n+k)$. 
\end{frame}

\begin{frame} 
  \frametitle{\huge COMPLEXITY OF SORTING}
  \tiny
  The number of comparisons needed to go from $n!$ unsorted lists to 1
  sorted list (assuming unique values, the worst-case difficulty),
  $\log_2(n!)$ comparisons are needed.

  \begin{eqnarray*}
    \log_2(n!) &=& \log_2(1) + \log_2(2) + \cdots + \log_2(n)\\
    &\leq& \log_2(n) + \log_2(n) + \cdots + \log_2(n)\\
    &=& n \log_2(n)\\
    &\in& O(n \log(n))\\
    \log_2(n!) &=& \log_2(1) + \log_2(2) + \cdots + \log_2(n)\\
    &\geq& \log_2\left(\frac{n}{2}\right) + \log_2\left(\frac{n}{2}+1\right) + \cdots + \log_2\left(n\right)\\
    &\geq& \log_2\left(\frac{n}{2}\right) + \log_2\left(\frac{n}{2}\right) + \cdots + \log_2\left(\frac{n}{2}\right)\\
    &=& \frac{n}{2} \log_2\left(\frac{n}{2}\right)\\
    &\in& \Omega(n \log(n))
  \end{eqnarray*}

  Sorting $n$ values is $\in \Theta(n \log(n))$. 
\end{frame}

\begin{frame} 
  \frametitle{\huge NAIVE APPROACH: SORTING}
  Generate $n^2$ values, sort, retrieve minimal $k$ values.\newline

  \[r(n) \in \Theta(n^2 \log(n))\]
\end{frame}

\begin{frame} 
  \frametitle{\Large IS SORTING \emph{NECESSARY} FOR SELECTION?}
  \footnotesize
  \begin{tabular}{rl}
    \tiny
    \begin{minipage}{2in}
      1D selection via ``median-of-medians'' (Blum \emph{et al.} 1973): Divide into lists of
      size 5. Get median of each in $O(1)$ time and get median ($O(n)$
      total). Compute median of $\frac{n}{5}$ medians, $\tau$ (shaded
      black).\newline

      There must be $\geq \frac{3 \frac{n}{5}}{2}$ values $\leq \tau$\\
      (the values that must be $\leq \tau$ are colored red; there may
      be other values $\leq \tau$).\\
      There must be $\geq \frac{3 \frac{n}{5}}{2}$
      values $\geq \tau$\\
      (the values that must be $\geq \tau$ are
      colored blue; there may be other values $\geq \tau$).
    \end{minipage}
    &
    \begin{minipage}{2in}
      \includegraphics[width=2in]{../figures/median-of-medians.pdf}
    \end{minipage}
  \end{tabular}\newline

  Iterate through and count values $\leq \tau$ and count values $\geq
  \tau$. The smaller of these counts cannot be less than
  $\frac{3n}{10}$. Either the values $\leq \tau$ or $\geq \tau$ can be
  removed without removing the true median. The algorithm therefore
  begins again from scratch on this new smaller list of size
  $\frac{7n}{10}$.\newline

  Runtime: $r(n) \leq n + r\left(\frac{n}{5}\right) + n +
  r\left(\frac{7n}{10}\right)$. Inductively, any recursion depth, the
  size of all child recursions relative to the size of all parent
  recursions is $\leq \frac{n}{5} + \frac{7n}{10} =
  \frac{9n}{10}$. Total work is thus $\leq 2n\cdot (1 + \frac{9}{10} +
       {\left(\frac{9}{10}\right)}^2 + \cdots) \in O(n)$.

\end{frame}

\begin{frame} 
  \frametitle{\large NAIVE APPROACH: MEDIAN-OF-MEDIANS SELECTION}
  Generate $n^2$ values, use median-of-medians to compute $k^{\text{th}}$ value, partition.\newline

  \[r(n) \in \Theta(n^2)\]
\end{frame}

\begin{frame} 
  \frametitle{\Large MORE ADVANCED APPROACH: SORTING AXES}
  \footnotesize
  \begin{tabular}{rl}
    \tiny
    \begin{minipage}{2in}
      Separately sort axes $X$ and $Y$ in $\Theta(n \log(n))$
      time. Best result is $X_1+Y_1$. Second best is either $X_1+Y_2$
      or $X_2+Y_1$. Proceed in this manner, only considering $X_i+Y_j$
      after $X_{i-1}+Y_j$ or $X_i+Y_{j-1}$ is already included in the
      result.\newline

      Gray pairs indicate values included in the result, black
      indicate values currently considered as the next value, and
      white indicate values not yet considered.
    \end{minipage}
    &
    \begin{minipage}{1in}
      \includegraphics[width=1in]{../figures/axes-sorted.pdf}
    \end{minipage}
  \end{tabular}\newline

  The ``hull'' (black squares) are stored in a heap as tuples
  $(X_i+B_j,i,j)$. At each iteration, pop the next smallest value from
  that heap and insert its neighbors $(X_{i+1}+B_j,i+1,j)$ and
  $(X_i+B_{j+1},i,j+1)$ (if either is already in the heap, don't
  insert it again).\newline

  After $k$ pops, the size of the hull is at most $t = \min(n, k+1)$; therefore, the runtime of each insert/pop operation to the hull was $\in O(\log(t))$, and thus the runtime is $\in O(n\log(n) + k\log(t)) = O(n\log(n) + k\log(\min(n,k)))$.\newline
\end{frame}

\begin{frame} 
  \frametitle{\Large HEAP APPROACH (WITH UNSORTED AXES)} \footnotesize
  ``Heapify'' $X$ and $Y$: w.l.o.g., $X_i \leq X_{2i},
  X_{2i+1}$. This is done in $O(n)$. Start with $(X_1+Y_1,1,1)$ (the
  best result value) in the heap (order tuples in the heap
  lexicographically). When a value is popped, insert its nearest
  inferior ``dependent'' tuples, taking care not to insert multiple
  times:
  \[
  dependents(i,j) = 
  \begin{cases}
    \{(2i,1), (2i+1,1), (i,2), (i,3)\}, & j=1\\
    \{(i,2j), (i,2j+1)\}, & j>1.\\
  \end{cases}.
  \]

  Each pop removes one value from the heap and inserts between two and
  four values; therefore, after $k$ pops, the size of the heap is $\in
  [k, 3k]$. The runtime of each insert/pop operation to the hull was
  $\in O(\log(\cdot))$, and thus the total runtime (including
  heapification) is $\in \Theta(n + k \log(k))$. Any method that
  retrieves the $k$ results in sorted order will be
  $\in\Omega(k\log(k))$; to get linear time in $n$ and $k$, don't sort
  axes, don't retrieve results in sorted order.
\end{frame}

\begin{frame} 
  \frametitle{\Large LAYER-ORDERED HEAP} \footnotesize

  \begin{tabular}{cc}
    {\bf HEAP} & {\bf LAYER-ORDERED HEAP}\\
    \includegraphics[width=2in]{../figures/heap.pdf} & \includegraphics[width=2in]{../figures/layer-ordered-heap.pdf}
  \end{tabular}

  In a heap, nodes can be better than the siblings of their parents
  (\emph{e.g.}, $4\leq 6$). In a layer-ordered heap, this isn't
  allowed: every element in level $\ell$ of the tree is $\leq$ to
  every element in level $\ell+1$.
\end{frame}

\begin{frame} 
  \footnotesize

  \frametitle{\Large LAYER-ORDERED HEAP} \footnotesize Define a
  ``layer-ordered heap'' (LOH) to be a stricter form of heap. Whereas
  a standard heap has $X_i \leq X_{2i}, X_{2i+1}$ (\emph{i.e.}, the
  children of $X_i$ are not better than $X_i$), there is no ordering
  between the left-left grandchild of $X_i$ and the right child of
  $X_i$.\newline

  A LOH partitions $X$ into layers, where each layer $L_i$ is a
  collection of values from $X$ and where each layer has $L_i \leq
  L_{i+1}$. A LOH of rank $\alpha$ has layers that grow exponentially
  in size: $\alpha \approx \frac{|L_{i+1}|}{|L_i|}$.

  For $\alpha \gg 1$ (\emph{e.g.}, $\alpha=2$), LOHify can be
  performed in $O(n)$: use median-of-medians to partition the largest
  layer of $X$ away from the remaining values. When $\alpha=2$, the
  largest layer will be slightly larger than the remaining
  values. Thus, the runtime from median-of-medians calls will be $\leq
  O\left(n + \frac{n}{2} + \frac{n}{4} + \cdots \right) \in
  O(n)$.\newline

  With $\alpha=2$, $(8,2,5,1,9,4,7)$ can be LOHified into $((1) \leq (4,2) \leq (5,9,8,7))$.\newline

  {\bf Formalized by Kreitzberg, Lucke, and Serang.}
\end{frame}

\begin{frame} 
  \frametitle{``BIRTHDAY PAPER''}

  \includegraphics[width=4in]{../figures/snowy.JPG}
\end{frame}

\begin{frame} 
  \frametitle{LOH ALGORITHM FOR SELECTION $k$ MIN FROM $X+Y$}
  \tiny

  \begin{tabular}{cc}
    \begin{minipage}{1.8in}
      \includegraphics[width=1.8in]{../figures/layer-product-grid.pdf}
    \end{minipage} &
    \begin{minipage}{2in}
      1. Separately LOHify $X$ and $Y$ with $\alpha=2$ ($O(n)$ steps). They
      will each have $m \approx \log_\alpha(n)$ layers. Compute the
      minimum and maximum values in each layer ($O(n)$ steps).\newline
      
      2. Build a grid of ``layer products'' (LPs).\newline
      
      3. Sort min and max corners of LPs ($O(m^2\log(m)) =
      O(\log^2_\alpha(n) \log(\log_\alpha(n))) \subset o(n)$
      steps). Iterate through until the area of the max corners is
      $\geq k$ ($O(m^2) = O(\log^2_\alpha(n)) \subset o(n)$ steps). Let $s \geq k$ be the area
      of these considered LPs.\newline

      4. Every min corner whose max corner is not counted in $s$ will
      also be considered. Use $s'$ to denote the number of these
      elements.\newline

      5. Generate all elements from these LPs. Perform a 1D
      $k$-selection on the result via median-of-medians ($O(n+k)$ steps).
    \end{minipage}
  \end{tabular}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=0<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-0.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=0<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-1.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-2.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-3.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-4.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=1<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-5.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=3<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-6.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=3<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-7.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-8.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-9.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-10.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=5<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-11.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=9<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-12.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=13<k$, continue\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-13.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  $s=17\geq k$, terminate.\\
  \includegraphics[width=2.8in]{../figures/k-is-26-animation-14.pdf}
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  \includegraphics[width=2.8in]{../figures/k-is-26-s.pdf}\newline

  First, we have largest value seen $\tau=10$, where at least $s=17$
  values are $\leq \tau$. Thus, $\tau \geq$ the $17^{\text{th}}$
  smallest value of $X+Y$ and $\tau \geq$ the $14^{\text{th}}$
  smallest value of $X+Y$.
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  \includegraphics[width=2.8in]{../figures/k-is-26-s.pdf}\newline

  Second, consider $(1,5)+(1)$ the LP above the final LP. Its area,
  $2$, is counted in the $13<k$. Also, our final LP has area
  $=\alpha\cdot 2$ = 4. Thus, we have $\text{area of final LP} =
  \alpha\cdot 2 < \alpha\cdot k$; therefore, $s\in \Theta(k)$.
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}
  \includegraphics[width=2.8in]{../figures/k-is-26-s-prime.pdf}\newline

  \tiny
  Consider, $s'=33$, the area of all LPs with any value $\leq
  \tau$. Consider the LP at layer index $(u,v)$, which is counted in
  $s'$.\newline

  {\bf Case: $u>1,v>1$}: We know that the sorted order must visit
  max-corner at $(u-1,v-1)$ before the min corner of $(u,v)$. Thus,
  the max-corner at $(u-1,v-1)$ must be counted in $s$. These LPs with
  min corner at $(u,v)$ and max corner at $(u,v)$ not counted by $s$
  will have area $\leq \alpha^2 s \in O(k)$.\newline

  {\bf Case: $u=1$ or $v=1$}: W.l.o.g., the max corner LP at $(u-1,1)$
  preceeds the min corner of $(u,1)$ in the sorted order. Visiting the
  max corner of $(u,1)$ counts its area in $s$. Thus, at most one LP
  with $v=1$ is counted by $s'$ but not by $s$, and LP $(u-1,1)$ must
  be counted by $s$. Thus, for both $u=1$ or $v=1$, these LPs will
  have area $\leq 2 \alpha\cdot s \in O(k)$.
\end{frame}

\begin{frame} 
  \frametitle{SELECTION ON $X+Y$ with LOH EXAMPLE $k=14$}

  The LPs guaranteed to contain the minimal $k$ values have total area
  $\in O(k)$. Generate all of their $O(k)$ values and select the
  minimal $k$ using median-of-medians.
\end{frame}

\begin{frame} 
  \frametitle{ALGORITHM FOR SELECTION ON $X+Y$ WITH LOH}
  \small
  {\bf PROOF OF CORRECTNESS AND RUNTIME:} \footnotesize Let $\delta$ denote the area
  of the LP that makes $s \geq k$, and let $\tau$ denote
  the maximum value in that LP. $s-\delta < k$ (or the
  algorithm would terminate earlier). There are $\geq k$ values $\leq
  \tau$. The true value of the $k^{\text{th}}$ smallest element $\tau'
  \leq \tau$, because $\tau'$ is defined as the minimal value that has
  $k$ values $\leq$ itself. Thus, only values $\leq \tau$ need be
  considered in the selection.\newline

  The area $\delta \leq \alpha\cdot (s-\delta) < \alpha\cdot k \in O(k)$;
  hence, $s = (s-\delta)+\delta < k+\delta \in O(k)$.\newline

  Let $s'$ be the area of all LPs not counted in $s$ with min corners
  with value $<\tau$. If LP $(u',v')$ is counted in $s'$, then either
  $u'=1$ or $v'=1$ or $\exists u,v$ counted in $s$ with max corner $<$
  the min corner of $(u',v')$. W.l.o.g., $v'=1$ can add only a single
  LP to $s'$, and with $(u'-1,1)$ counted by $s$. Thus, the $v'=1$
  case adds at most $\alpha\cdot k$ to $s'$ and $u'=1,v'=1$ adds at
  most $2 \alpha\cdot k$ to $s'$. $s'\in O(k)$.\newline

  Median-of-medians runs on $s+s'$ values; the total runtime is
  $O(n+m^2\log(m)+m^2+s+s')=O(n+k)$.
\end{frame}

\begin{frame} 
  \frametitle{\large $X+Y$ selection code}

  \includegraphics[width=4in]{../figures/x-plus-y-selection-code.png}
\end{frame}
  
\begin{frame} 
  \frametitle{\large EMPIRICAL RUNTIMES}
  \tiny
\begin{tabular}{ l | c c c c }
& Naive $n^2\log(n)$ & Kaplan \emph{et al.} & Layer-ordered heap (total=LOHify+solve)\\
  \hline
$n=1000,k=250$ & 0.939 & 0.0511 & 0.00892=0.00693+0.002\\
$n=1000,k=500$ & 0.952 & 0.099 & 0.0102=0.00648+0.00374\\
$n=1000,k=1000$ & 0.973 & 0.201 & 0.014=0.00764+0.00639\\
$n=1000,k=2000$ & 0.953 & 0.426 & 0.0212=0.00652+0.0146\\
$n=1000,k=4000$ & 0.950 & 0.922 & 0.0278=0.00713+0.0206\\
\hline
$n=2000,k=500$ & 4.31 & 0.104 & 0.0194=0.0160+0.00342\\
$n=2000,k=1000$ & 4.11 & 0.203 & 0.0211=0.0139+0.00728 \\
$n=2000,k=2000$ & 4.17 & 0.432 & 0.0254=0.0140+0.0114\\
$n=2000,k=4000$ & 4.16 & 0.916 & 0.0427=0.0147+0.0280\\
$n=2000,k=8000$ & 4.13 & 2.03 & 0.0761=0.0143+0.0617\\
\hline
$n=4000,k=1000$ & 17.2 & 0.207 & 0.0507=0.0459+0.00488\\
$n=4000,k=2000$ & 17.2 & 0.422 & 0.408=0.0268+0.0141\\
$n=4000,k=4000$ & 17.1 & 0.907 & 0.0481=0.0277+0.0205\\
$n=4000,k=8000$ & 17.3 & 1.98 & 0.0907=0.0278+0.0629\\
$n=4000,k=16000$ & 17.3 & 4.16 & 0.133=0.0305+0.103\\
\end{tabular}
\vspace{0.25in}

In C++, $n=4000,k=16000$ takes 0.00175s total.\newline
In C++, $n=16000,k=64000$ takes 0.0.00516s total.\newline
In C++, $n=2^{16},k=2^{20}$ takes 0.0450s total.\newline
In C++, $n=2^{20},k=2^{24}$ takes 0.543s total.\newline
\end{frame}

\begin{frame} 
  \frametitle{\large GENERATING MOST ABUNDANT ISOTOPE PEAKS}

  Build a tree of $X+Y$ nodes. At the leaves, generate multinomial
  terms. \emph{E.g.}, an element with four Carbons has polynomial
  $(0.9893\cdot X^{12.0} + 0.0107\cdot X^{13.003})^4$.\newline

  (Live demo of {\tt NeutronStar}, the world's fastest isotope calculator) 
\end{frame}

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  Patrick Kreitzberg (PhD student in mathematics)\newline

  Patrick and I worked late into several nights to develop the theory
  that uses $\alpha<2$ to produce a $o(n\cdot m + k\cdot m)$ method
  for selection on $X_1+X_2+\cdots+X_m$. Consider that loading the
  data costs $n\cdot m$ and computing $k$ results by looking up the
  proper index in each of $X_1, X_2, \ldots X_m$ would cost $k\cdot
  m$; this is a remarkable result.\newline

  This discovery made possible the approaches used in {\tt
    NeutronStar} to generate the most abundant isotopes.\newline

  Patrick works primarily on combinatorial algorithms. 
\end{frame} 

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  \includegraphics[width=4.25in]{../figures/patrick-kreitzberg.JPG}

  Patrick Kreitzberg (PhD student in mathematics)
\end{frame} 

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  Kyle Lucke (MA student in computer science)\newline
  
  Kyle and I wrote the first versions of some of the
  $X_1+X_2+\cdots+X_m$ algorithms in Python. These were part of
  developing the $o(m\cdot n + k\cdot m)$ algorithm.\newline

  Kyle works mostly on combinatorial problems from protein inference
  from mass spectrometry, including graphical models and statistical
  validation.
\end{frame}

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  \includegraphics[width=2.5in]{../figures/kyle-lucke.JPG}

   Kyle Lucke (masters student in computer science)
\end{frame} 

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  Jake Pennington (MA student in mathematics)\newline

  Jake and I created an optimal algorithm to construct a LOH when
  $\alpha \ll 2$. Jake used information theory to bound the best-case
  runtime of LOH construction, and then proved one algorithm has
  optimal runtime, regardless of $\alpha$. Consider, selecting away
  the largest layer (presented here) will become quadratic when
  $\alpha=1$; $\alpha=1$ corresponds to a sorted order, and thus,
  should have an optimal runtime of $n\log(n)$. Bounding the runtime
  was tricky, because neither master theorem nor Akra-Bazzi solve the
  recurrence. Jake also helped prove the correctness of the
  multinomial generators for {\tt NeutronStar}.\newline

  Jake's work focuses on applied math and computational combinatorics
  problems.
\end{frame}

\begin{frame} 
  \frametitle{\large MY EXCEPTIONAL STUDENTS}
  \footnotesize
  \includegraphics[width=3.25in]{../figures/jake-pennington.JPG}

   Jake Pennington (masters student in mathematics)
\end{frame} 

\begin{frame} 
  \frametitle{\large OUR PODCAST: EXPLORING SCIENTIFIC WILDERNESS}

  \includegraphics[width=4in]{../figures/exp-sci-wild.jpg}
\end{frame}

\begin{frame} 
  \frametitle{THANKS TO ITALIANS DO IT BETTER}
  \footnotesize
  The greatest Euro disco record label in the world.\newline

  Megan Louise and Johnny Jewel, you are true soul surfers.\newline
  
  Recommended starting tracks: \emph{Under Your Spell} and \emph{Tears in Heaven} (by Desire) and \emph{Kill for Love} (by Chromatics).\newline
  \center
    \includegraphics[width=2in]{../figures/italians-do-it-better.jpg}
\end{frame}

\end{document}
