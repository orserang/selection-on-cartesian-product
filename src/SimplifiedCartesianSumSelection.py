from LayerOrderedHeap import *

class SimplifiedCartesianSumSelection:
  def _min_tuple(self,i,j):
    # True for min corner, False for max corner
    return (self._loh_a.min(i) + self._loh_b.min(j), (i,j), False)

  def _max_tuple(self,i,j):
    # True for min corner, False for max corner
    return (self._loh_a.max(i) + self._loh_b.max(j), (i,j), True)

  def __init__(self, array_a, array_b):
    self._loh_a = LayerOrderedHeap(array_a)
    self._loh_b = LayerOrderedHeap(array_b)

    self._full_cartesian_product_size = len(array_a) * len(array_b)

    self._sorted_corners = sorted([self._min_tuple(i,j) for i in range(len(self._loh_a)) for j in range(len(self._loh_b))] + [self._max_tuple(i,j) for i in range(len(self._loh_a)) for j in range(len(self._loh_b))])

  def select(self, k):
    assert( k <= self._full_cartesian_product_size )
    
    candidates = []
    index_in_sorted = 0
    num_elements_with_max_corner_popped = 0
    while num_elements_with_max_corner_popped < k:
      val, (i,j), is_max = self._sorted_corners[index_in_sorted]
      new_candidates = [ v_a+v_b for v_a in self._loh_a[i] for v_b in self._loh_b[j] ]
      if is_max:
        num_elements_with_max_corner_popped += len(new_candidates)
      else:
        # Min corners will be popped before corresponding max corner;
        # this gets a superset of what is needed (just as in phase 2)
        candidates.extend(new_candidates)
      index_in_sorted += 1
    
    print( 'Ratio of total popped candidates to k: {}'.format(len(candidates) / k) )
    k_small_vals, large_vals = partition(candidates, k)
    return k_small_vals
