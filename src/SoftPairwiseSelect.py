import heapq
from one_dimensional_selection import *
import time
from soft_heap import *

class SoftPairwiseSelect:
  def __init__(self, a_lst, b_lst):
    heapq.heapify(a_lst)
    heapq.heapify(b_lst)
    self._a_lst = a_lst
    self._b_lst = b_lst
    self._size = len(a_lst)*len(b_lst)

    self._soft_heap = make_heap()
    self._items_to_be_inserted = []
    self._corrupted_items = []
    self._output_list = []
    self._size_of_soft_heap = 0
    self._T = 2.0 # same as epsilon = 1/4 

    # Insert initial items in soft heap
    first_item = Item( (self._a_lst[0][0] + self._b_lst[0][0]), (0,0) )
    first_item.corrupt = True # If false, then will have children & be put in output_list multiple times
    self._soft_heap = sh_insert(first_item, self._soft_heap, self._corrupted_items, self._T)
    self._size_of_soft_heap += 1
    self._output_list.append( (first_item.key, first_item.data) )
    self._insert_children_into_soft_heap(0, 0)

  def select(self, k):
    k = min(k, len(self))
    for i in range(k):
      if self._size_of_soft_heap > 0:
        result_item, self._soft_heap = extract_min(self._soft_heap, self._corrupted_items, self._T)
        self._size_of_soft_heap -= 1
      
        if not result_item.corrupt:
          self._corrupted_items.append( result_item )
        
        for itm in self._corrupted_items:
          i,j = itm.data
          self._insert_children_into_soft_heap(i, j) # Appends to output list and to items_to_be_inserted       
        self._corrupted_items = []

      for itm in self._items_to_be_inserted:
        self._soft_heap = sh_insert(itm, self._soft_heap, self._corrupted_items, self._T)
        self._size_of_soft_heap += 1
      self._items_to_be_inserted = []

    good_items, bad_items = soft_select_and_return_unused_values(self._output_list, k)
    self._output_list = bad_items
    return [(key, data) for key, data in good_items]
    
  def __len__(self):
    if self._size > 2**32-1:
      return 2**32-1
    return self._size
  
  def _insert_children_into_soft_heap(self, i, j):
    if j == 0:
      self._insert_children_when_y_is_0(i)
    else:
     self._insert_children_when_y_greater_than_zero(i,j)

  def _insert_if_in_bounds(self, i, j):
    if  i  < len(self._a_lst) and j < len(self._b_lst):
      key_a, data_a = self._a_lst[i]
      key_b, data_b = self._b_lst[j]
      key = key_a+key_b
      data = (i, j)
      self._items_to_be_inserted.append( Item(key, data) )
      self._output_list.append( (key, data) )

  def _insert_children_when_y_is_0(self, i):
    self._insert_if_in_bounds(left_child(i), 0)
    self._insert_if_in_bounds(right_child(i), 0)
    self._insert_if_in_bounds(i, 1)
    self._insert_if_in_bounds(i, 2)

  def _insert_children_when_y_greater_than_zero(self, i, j):
    self._insert_if_in_bounds(i, left_child(j))
    self._insert_if_in_bounds(i, right_child(j))
