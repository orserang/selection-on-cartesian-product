def naive_cartesian_select(a, b, k):
  return sorted([ val_a + val_b for val_a in a for val_b in b ])[:k]

