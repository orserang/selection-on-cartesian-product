from soft_heap import *
import numpy as np
import math

def left_child(index):
  return 2*index+1

def right_child(index):
  return 2*index+2

def check_list_is_in_min_heap_order(x):
  # Input should be heapified in min-heap form
  # To check, for x_i, x_2i+1 and x_2i+2 should be greater than x
  for i in range(len(x)):
    if left_child(i) < len(x) and x[left_child(i)] < x[i]:
      return False
    if right_child(i) < len(x) and x[right_child(i)] < x[i]:
      return False
  return True

def sort_select(v, k):
  result = sorted(v)[:k]
  np.random.shuffle(result)
  return result

def sort_select_into_good_and_bad(v, k):
  result = sorted(v)
  good = result[:k]
  bad = result[k:]
  np.random.shuffle(good)
  np.random.shuffle(bad)
  return good, bad

def select_k_leq_key_and_remainders(key_data_pairs, key_value, k):
  selected = []
  not_selected  = []
  leftovers = []
  for key, data in key_data_pairs:
    if key < key_value:
      selected.append( (key, data) )
    elif key > key_value:
      not_selected.append( (key, data) )
    else:
      leftovers.append( (key, data) )

  number_left_to_select = k-len(selected)
  selected.extend( leftovers[:number_left_to_select] )
  not_selected.extend(leftovers[number_left_to_select:])
  return (selected, not_selected)

def partition(key_data_pairs, pivot_val):
  i = -1
  j = len(key_data_pairs)
  while True:
    # do while
    i += 1
    key, data = key_data_pairs[i]
    while i < len(key_data_pairs) and key < pivot_val:      
      i += 1
      key, data = key_data_pairs[i]
      
    # do while
    j -= 1
    key, data = key_data_pairs[j]    
    while j >=0 and key > pivot_val:
      j -= 1
      key, data = key_data_pairs[j]          
    if i>=j:
      return j
    key_data_pairs[i], key_data_pairs[j] = key_data_pairs[j], key_data_pairs[i]


def soft_select_helper(key_data_pairs, k):
  if k == 0:
    return min(key_data_pairs)
  if k == len(key_data_pairs):
    return max(key_data_pairs)

  n = len(key_data_pairs)
  epsilon = 1.0/3.0
  T = np.log2(1.0/epsilon)
  soft_heap = make_heap()
  for i in range(n):
    key, data = key_data_pairs[i]
    soft_heap = sh_insert(Item(key, data), soft_heap, [], T)

  max_popped_uncorrupted_key = -math.inf
  maximum_num_corrupted_items = int(math.ceil(epsilon*n))

  all_popped_items_from_soft_heap = []
  for i in range(maximum_num_corrupted_items):
    item, soft_heap = extract_min(soft_heap, [], T)
    key, data = item.key, item.data
    max_popped_uncorrupted_key = max(max_popped_uncorrupted_key, key)

  partition_index = partition(key_data_pairs, max_popped_uncorrupted_key) + 1
  lower = key_data_pairs[:partition_index]
  upper = key_data_pairs[partition_index:]
  if k <= partition_index:
    return soft_select_helper(lower, k)
  return soft_select_helper(upper, k - partition_index)
  
def soft_select_and_return_unused_values(key_data_pairs, k):
  if k > len(key_data_pairs):
    return (key_data_pairs, [])

  if k > len(key_data_pairs):
    return (key_data_pairs, [])

  key_data_at_k = soft_select_helper(key_data_pairs, k)
  key, data = key_data_at_k
  good_items, bad_items = select_k_leq_key_and_remainders(key_data_pairs, key, k)
  assert(len(good_items) + len(bad_items) == len(key_data_pairs))

  return (good_items, bad_items)

def soft_select(key_data_pairs, k):
  good_items, bad_items = soft_select_and_return_unused_values(key_data_pairs, k)
  return good_items

def soft_select_value_at_k(key_data_pairs, k):
  if k > len(key_data_pairs):
    return max(key_data_pairs[0])
  key_data_at_k = soft_select_helper(key_data_pairs, k)
  return key_data_at_k

def multi_select_helper(key_data_pairs, unique_sorted_selection_ks, result):
  n = len(key_data_pairs)
  if n in (0,1) or len(unique_sorted_selection_ks) == 0:
    result.extend(key_data_pairs)
    return

  if len(unique_sorted_selection_ks) == 1 and unique_sorted_selection_ks[0] == n:
    result.extend(key_data_pairs)
    return

  # choose k that bisects remaining selection ks:
  index_in_unique_sorted_selection_ks = len(unique_sorted_selection_ks) // 2 - 1
  if index_in_unique_sorted_selection_ks < 0:
    index_in_unique_sorted_selection_ks = 0
  
  left_unique_sorted_selection_ks, right_unique_sorted_selection_ks = unique_sorted_selection_ks[:index_in_unique_sorted_selection_ks+1], unique_sorted_selection_ks[index_in_unique_sorted_selection_ks+1:]
  selection_k = unique_sorted_selection_ks[index_in_unique_sorted_selection_ks]
  left_key_data_pairs, right_key_data_pairs = soft_select_and_return_unused_values(key_data_pairs, selection_k)
  multi_select_helper(left_key_data_pairs, left_unique_sorted_selection_ks, result)
  multi_select_helper(right_key_data_pairs, right_unique_sorted_selection_ks - len(left_key_data_pairs), result)

def multi_select(key_data_pairs, unique_sorted_selection_ks):
  result = []
  multi_select_helper(key_data_pairs, unique_sorted_selection_ks, result)
  return result
