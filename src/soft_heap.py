# A python implementation of the soft heaps described in the paper
# "Soft Heaps Simplified", by Haim Kaplan, Robert E. Tarjan and Uri Zwick.
# (c) Haim Kaplan, Robert E. Tarjan and Uri Zwick.

# Altered to do lazy inserting and return corrupted items when
# extracting the min value (in order to use soft-select algorithm from
# "Selection from heaps, row-sorted matrices and X + Y using soft
# heaps" by Kaplan et. al.

INF = float('inf')
########
# Item #
########

class Item:
  def __init__(e, it, data):
    e.key = it
    e.next = e
    e.data = data
    e.corrupt = False

########
# Node #
########
    
class Node:
  pass

null = Node()
null.set = null
null.key = INF
null.rank = INF
null.left = null
null.right = null
null.next = null

##########
# defill #
##########

def defill(x, corrupted_items, T):
  fill(x, corrupted_items, T)
  if x.rank > T and x.rank%2 == 0 and x.left != null:
    fill(x, corrupted_items, T)

########
# fill #
########

def fill(x, corrupted_items, T):
  if x.left.key > x.right.key:
    x.left, x.right = x.right, x.left

  # Corruption happens here
  if x.key != x.left.key:
    first_item = x.set
    if first_item != null and not first_item.corrupt:
      first_item.corrupt = True
      corrupted_items.append(first_item)
    itm = first_item.next
    while itm != first_item:
      if not itm.corrupt:
        itm.corrupt = True
        corrupted_items.append(itm)
      itm = itm.next

  x.key = x.left.key
  if x.set == null:
    x.set = x.left.set
  else:
    x.set.next, x.left.set.next = x.left.set.next, x.set.next
  x.left.set = null
  if x.left.left == null: 
    # destroy x.left
    x.left = x.right
    x.right = null
  else:
    defill(x.left, corrupted_items, T)
    
#############
# make_heap #
#############

def make_heap():
  return null

############
# find_min #
############

def find_min(H):
  return (H.set.next, H.key)

#############
# rank_swap #
#############

def rank_swap(H):
  x = H.next
  if H.rank <= x.rank: 
    return H
  else:
    H.next = x.next
    x.next = H 
    return x

############
# key_swap #
############

def key_swap(H):
  x = H.next

  if H.key <= x.key: 
    return H
  else:
    H.next = x.next
    x.next = H
    return x

##############
# delete_min #
##############

def delete_min(H, corrupted_items, T):
  e = H.set.next
  if e.next != e: 
    H.set.next = e.next
    return H
  else:
    H.set = null
    k = H.rank
    if H.left == null: 
      L = H.next
      # destroy H
      H = L
    else:
      defill(H, corrupted_items, T)
    return reorder(H, k)
  
###########
# reorder #
###########

def reorder(H, k):
  if H.next.rank < k: 
    H = rank_swap(H)
    H.next = reorder(H.next, k)
  return key_swap(H)

##########
# insert #
##########

def sh_insert(e, H, corrupted_items, T):
  return key_swap(meldable_insert(make_root(e), rank_swap(H), corrupted_items, T)) 

#############
# make_root #
#############

def make_root(e):
  x = Node()
  e.next = e
  x.set = e
  x.key = e.key
  x.rank = 0
  x.left = null
  x.right = null
  x.next = null
  return x

###################
# meldable_insert #
###################

def meldable_insert(x, H, corrupted_items, T):
  if x.rank < H.rank: 
    x.next = key_swap(H)
    return x
  else:
    return meldable_insert(link(x, H, corrupted_items, T), rank_swap(H.next), corrupted_items, T)

########
# link #
########

def link(x, y, corrupted_items, T):
  z = Node()
  z.key = INF
  z.set = null
  z.rank = x.rank + 1
  z.left = x
  z.right = y
  defill(z, corrupted_items, T)
  return z

def extract_min(H, corrupted_items, T):
  result = find_min(H)[0]
  H = delete_min(H, corrupted_items, T)
  return result, H
