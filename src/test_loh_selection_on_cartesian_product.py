from CartesianSumSelection import *
from SimplifiedCartesianSumSelection import *
from SoftPairwiseSelect import *
from naive_cartesian_select import *
import numpy as np
import sys
from time import time

def add_indices(x):
  return [ (b,a) for a,b in enumerate(x) ]

def remove_indices(x):
  return [ a for a,b in x ]

def main(args):
  if len(args) != 2:
    print('usage: <n> <k>')
  else:
    n, k = [ int(x) for x in args ]
    max_int = 2**14
    a = np.random.randint(0, max_int, n)
    b = np.random.randint(0, max_int, n)

    t1=time()
    naive_result = naive_cartesian_select(a,b,k)
    t2=time()
    print('Naive took {0:.3g} seconds'.format(t2-t1))
    print()

    t1=time()
    sps = SoftPairwiseSelect(add_indices(a),add_indices(b))
    soft_result = remove_indices(sps.select(k))
    t2=time()
    print('Soft heap took {0:.3g} seconds'.format(t2-t1))
    if sorted(soft_result) == sorted(naive_result):
      print( 'SOFT PASS' )
    else:
      print( 'SOFT FAIL' )
    print()

    t1=time()
    loh_css = SimplifiedCartesianSumSelection(a,b)
    t2=time()
    loh_result = loh_css.select(k)
    t3=time()
    print('LOH took {0:.3g}={1:.3g}+{2:.3g} seconds'.format(t3-t1, t2-t1, t3-t2))

    t1=time()
    loh_css = SimplifiedCartesianSumSelection(a,b)
    t2=time()
    simp_loh_result = loh_css.select(k)
    t3=time()
    print('LOH (simplified) took {0:.3g}={1:.3g}+{2:.3g} seconds'.format(t3-t1, t2-t1, t3-t2))

    if sorted(loh_result) == sorted(naive_result) and sorted(simp_loh_result) == sorted(naive_result):
      print( 'LOH PASS' )
    else:
      print( 'LOH FAIL' )

if __name__=='__main__':
  main(sys.argv[1:])

